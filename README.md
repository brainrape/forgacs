# Kinetic Installation Control


## Bootstrap

Set up the three Raspberry PIs

- for `num` in 1, 2, 3

    - flash new SD card
    - boot
    - log in with user pi, password raspberry
    - connect wifi
    - enable openssh

        `sudo systemctl start ssh`
    - get the node's `<ip>`

        `hostname -I`

    - deploy. from laptop, run

        `TARGET=pi@<ip> ./deploy <num>`

    - when you get lines with `INFO forgacs ...`, disconnect with `Ctrl`+`C`; the app will keep running


## Upload, Build and Run

`TARGET=pi@<ip> ./run <num>

Open the control interface at `http://<ip>/x5q6rzvd` in a browser.


## Operate

- plug in the pi
- wait until the screen goes black
- push the button
  - the machine will calibrate itself and start playing
- push the button for 1 second
  - the machine will stop the video and go back to resting position. it is now safe to unplug


### Emergency Stop

When in motion, a short button push will stop all motors and the video.


## Upload video

- copy a file named `video.mp4` to a thumb drive
- unplug the pi
- put the thumb drive in the pi
- power the pi
- the video is copied to internal storage
- when the screen goes black, remove the thumb drive
