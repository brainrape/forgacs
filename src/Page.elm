module Page exposing (..)

import Browser.Navigation
import Data exposing (..)
import Json.Decode as D
import Json.Encode as E
import Ports
import Time exposing (Posix)


type Msg
    = Recv (Maybe String)
    | Control ControlMsg
    | Servo Id String
    | StepPeriod Id String
    | Tick Posix
    | Reload


type alias Model =
    { state : Result String State
    , gotMsg : Bool
    }


initModel : Model
initModel =
    --{ state = Ok initState
    { state = Err "Not loaded"
    , gotMsg = True
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Recv Nothing ->
            ( { model | state = Err "Connection Closed" }, Cmd.none )

        Recv (Just val) ->
            ( { model | state = D.decodeString decodeState val |> Result.mapError Debug.toString, gotMsg = True }, Cmd.none )

        Control ctrl ->
            ( model, Ports.send (controlMsgToString ctrl) )

        Servo id str ->
            ( model
            , String.toFloat str
                |> Maybe.map
                    (\val ->
                        Ports.send (controlMsgToString (Data.Servo id val))
                    )
                |> Maybe.withDefault Cmd.none
            )

        StepPeriod id str ->
            ( model
            , String.toFloat str
                |> Maybe.map
                    (\val ->
                        Ports.send (controlMsgToString (Data.StepperPeriod id (round (10 ^ val))))
                    )
                |> Maybe.withDefault Cmd.none
            )

        Tick t ->
            case ( model.state, model.gotMsg ) of
                ( Ok _, False ) ->
                    ( { model | state = Err "Timed Out" }, Cmd.none )

                ( _, True ) ->
                    ( { model | gotMsg = False }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        Reload ->
            ( model, Browser.Navigation.reloadAndSkipCache )
