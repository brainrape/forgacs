use super::sensor;
use super::servo;
use super::stepper;
use super::switch;
use super::video;
use serde::{Deserialize, Serialize};
use tracing::{info, warn};

use super::performance::PERF;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum Msg {
    ToStage(Stage),
    ControlMsg(ControlMsg),
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum ControlMsg {
    Init(),
    Calibrate(),
    Reset(),
    Play1(),
    Play2(),
    Servo(u8, f64),
    Switch(u8, bool),
    StepperMsg(u8, stepper::Msg),
    VideoPlay(u8),
    VideoStop(u8),
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum Stage {
    Init(),
    Calibrate(),
    Ready(),
    Reset(),
    Play1(),
    Play2(),
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct State {
    pub hostname: String,
    pub ip: String,
    pub time: u64,
    pub stage: Stage,
    pub started: bool,
    pub servo1: servo::State,
    pub sensor1: sensor::State,
    pub sensor2: sensor::State,
    pub switch1: switch::State,
    pub switch2: switch::State,
    pub stepper1: stepper::State,
    pub stepper2: stepper::State,
    pub video1: video::State,
}

fn stepper_ready(s: &stepper::State, start: i64) -> bool {
    !s.exists || (s.calibrated && s.position == start && s.dir == None)
}

#[allow(dead_code)]
fn servo_ready(s: &servo::State, start: f64) -> bool {
    s.position == start && s.motion == None
}

impl State {
    pub fn new() -> State {
        State {
            hostname: String::from(""),
            ip: String::from(""),
            time: 0,
            stage: Stage::Init(),
            started: false,
            servo1: servo::State::new(),
            sensor1: sensor::State::new(),
            sensor2: sensor::State::new(),
            switch1: switch::State::new(),
            switch2: switch::State::new(),
            stepper1: stepper::State::new(),
            stepper2: stepper::State::new(),
            video1: video::State::new(),
        }
    }

    pub fn is_reset(&self) -> bool {
        return true
            // && !&self.video1.playing
            && stepper_ready(&self.stepper1, PERF.stepper1_start)
            && stepper_ready(&self.stepper2, PERF.stepper2_start); //&& servo_ready(&self.servo1, PERF.servo1_start);
    }

    pub fn is_calibrated(&self) -> bool {
        self.stepper1.calibrated && self.stepper2.calibrated
    }

    // pub fn is_done_play1(&self) -> bool {
    //     return true
    //         && stepper_ready(&self.stepper1, &self.stepper1.length - PERF.stepper1_end)
    //         //&& stepper_ready(
    //         //     &self.stepper2,
    //         //     PERF.stepper2_start + &self.stepper2.length / 2,
    //         // )
    //         //&& servo_ready(&self.servo1, PERF.servo1_end);
    //         ;
    // }

    // pub fn is_done_play2(&self) -> bool {
    //     return true
    //         && stepper_ready(&self.stepper1, PERF.stepper1_start)
    //         && stepper_ready(&self.stepper2, PERF.stepper2_start + &self.stepper2.length);
    //     //&& servo_ready(&self.servo1, PERF.servo1_start);
    // }
}

fn parse<T: std::str::FromStr>(str_index: &str, str_value: &str) -> Option<(u8, T)>
where
    <T as std::str::FromStr>::Err: std::fmt::Debug,
{
    match str_index.parse::<u8>() {
        Ok(i) => match str_value.parse::<T>() {
            Ok(x) => {
                return Some((i, x));
            }
            Err(err) => {
                warn!("Step: error parsing '{:?}': {:?}", str_value, err);
            }
        },
        Err(err) => {
            warn!("Step: error parsing number '{:?}'': {:?}", str_index, err);
        }
    }
    return None;
}

pub fn decode(msg: &str) -> Option<ControlMsg> {
    if msg == "" {
        warn!("empty msg");
    } else if msg == "GoInit" {
        return Some(ControlMsg::Init());
    } else if msg == "GoCalibrate" {
        return Some(ControlMsg::Calibrate());
    } else if msg == "GoReset" {
        return Some(ControlMsg::Reset());
    } else if msg == "GoPlay" {
        return Some(ControlMsg::Play1());
    } else if msg.starts_with("Servo ") {
        return parse::<f64>(&msg[6..7], &msg[8..]).map(|(i, v)| ControlMsg::Servo(i, v));
    } else if msg.starts_with("StepperPeriod ") {
        if let Some((i, x)) = parse::<u64>(&msg[14..15], &msg[16..]) {
            if (i == 1 && x >= 100 && x <= 10000) || (i == 2 && x >= 100 && x <= 10000) {
                return Some(ControlMsg::StepperMsg(i, stepper::Msg::Period(x)));
            } else {
                info!("Value out of bounds for StepperPeriod {:?}: {:?}.", i, x);
            }
        }
    } else if msg == "StepperMoveDir 1 (Just False)" {
        return Some(ControlMsg::StepperMsg(
            1,
            stepper::Msg::MoveDir(Some(false)),
        ));
    } else if msg == "StepperMoveDir 1 (Just True)" {
        return Some(ControlMsg::StepperMsg(1, stepper::Msg::MoveDir(Some(true))));
    } else if msg == "StepperMoveDir 1 Nothing" {
        return Some(ControlMsg::StepperMsg(1, stepper::Msg::MoveDir(None)));
    } else if msg == "StepperCalibrate 1" {
        return Some(ControlMsg::StepperMsg(1, stepper::Msg::Calibrate()));
    } else if msg == "StepperMoveDir 2 (Just False)" {
        return Some(ControlMsg::StepperMsg(
            2,
            stepper::Msg::MoveDir(Some(false)),
        ));
    } else if msg == "StepperMoveDir 2 (Just True)" {
        return Some(ControlMsg::StepperMsg(2, stepper::Msg::MoveDir(Some(true))));
    } else if msg == "StepperMoveDir 2 Nothing" {
        return Some(ControlMsg::StepperMsg(2, stepper::Msg::MoveDir(None)));
    } else if msg == "StepperCalibrate 2" {
        return Some(ControlMsg::StepperMsg(2, stepper::Msg::Calibrate()));
    } else if msg == "Switch 1 True" {
        return Some(ControlMsg::Switch(1, true));
    } else if msg == "Switch 1 False" {
        return Some(ControlMsg::Switch(1, false));
    } else if msg == "Switch 2 True" {
        return Some(ControlMsg::Switch(2, true));
    } else if msg == "Switch 2 False" {
        return Some(ControlMsg::Switch(2, false));
    } else if msg == "VideoPlay 1" {
        return Some(ControlMsg::VideoPlay(1));
    } else if msg == "VideoStop 1" {
        return Some(ControlMsg::VideoStop(1));
    } else {
        warn!("could not decode msg");
    }
    return None;
}
