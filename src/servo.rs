extern crate core_affinity;
use rppal::pwm::{Channel, Polarity, Pwm};
use serde::{Deserialize, Serialize};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::time::{Duration, SystemTime};
use tracing::{info, warn};

const PERIOD_MS: u64 = 20;
const PULSE_MIN_US: u64 = 1000;
const _PULSE_NEUTRAL_US: u64 = 1500;
const PULSE_MAX_US: u64 = 2000;

pub struct Servo {
    pub channel: Channel,
    pub tx: Sender<Msg>,
    pub rx: Receiver<State>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum Msg {
    GoTo(f64, Duration),
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct State {
    pub position: f64,
    pub motion: Option<Motion>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct Motion {
    pub position_start: f64,
    pub position_end: f64,
    pub time_start: SystemTime,
    pub duration: Duration,
}

impl State {
    pub fn new() -> State {
        State {
            position: 0.5,
            motion: None,
        }
    }
}

impl Servo {
    pub fn new(channel: Channel) -> Servo {
        let (tx_msg, rx_msg) = mpsc::channel();
        let (tx_state, rx_state) = mpsc::channel();

        let pwm = Pwm::with_period(
            channel,
            Duration::from_millis(PERIOD_MS),
            Duration::from_micros(PULSE_MAX_US),
            Polarity::Normal,
            true,
        )
        .unwrap();

        thread::spawn(move || {
            core_affinity::set_for_current(core_affinity::CoreId { id: 2 });

            let mut now;
            let mut state = State::new();
            loop {
                now = SystemTime::now();
                if let Ok(Msg::GoTo(position_end, duration)) = rx_msg.try_recv() {
                    if position_end >= 0.0 && position_end <= 1.0 {
                        state.motion = Some(Motion {
                            position_start: state.position,
                            position_end: position_end,
                            time_start: now,
                            duration: duration,
                        });
                        info!("{:?}", state);
                    } else {
                        warn!("position_end out of range: {:?}", position_end);
                    }
                }
                if let Some(motion) = state.motion {
                    if let Ok(elapsed) = now.duration_since(motion.time_start) {
                        if elapsed < motion.duration {
                            let progress =
                                elapsed.as_nanos() as f64 / motion.duration.as_nanos() as f64;
                            let motion_length = motion.position_end - motion.position_start;
                            state.position = motion.position_start + motion_length * progress;
                        } else {
                            state = State {
                                position: motion.position_end,
                                motion: None,
                            };
                        }
                    } else {
                        state.motion = None;
                    }
                    // send state
                    tx_state.send(state.clone()).unwrap();

                    // output
                    let progress_micros: u64 =
                        (state.position * (PULSE_MAX_US - PULSE_MIN_US) as f64) as u64;
                    let pulse_micros: u64 = PULSE_MIN_US + progress_micros;
                    let pulse_width = Duration::from_micros(pulse_micros);
                    pwm.set_pulse_width(pulse_width).unwrap();
                }
                thread::sleep(Duration::from_millis(10));
            }
        });

        return Servo {
            channel: channel,
            tx: tx_msg,
            rx: rx_state,
        };
    }
}
