extern crate core_affinity;
use rppal::gpio::Gpio;
use rppal::pwm::Channel;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tracing::info;
mod cmds;
mod data;
mod performance;
mod sensor;
mod servo;
mod stepper;
mod switch;
mod video;
mod web;
use data::{ControlMsg, Msg, Stage};
use performance::PERF;
use sensor::Sensor;
use stepper::Stepper;
use switch::Switch;
use video::Video;
use web::Web;

fn main() {
    //
    // init
    //
    tracing_subscriber::fmt::Subscriber::builder()
        .with_max_level(tracing::Level::INFO /*TRACE*/)
        .init();

    core_affinity::set_for_current(core_affinity::CoreId { id: 1 });

    let web = Web::new();

    let hostname_ = std::fs::read_to_string("/etc/hostname").unwrap();
    let hostname = hostname_.trim();
    info!("hostname: {:?}", hostname);

    let ip = cmds::get_ip();
    info!("ip: {:?}", ip);

    info!("Initializing GPIO.");
    let gpio = Gpio::new().unwrap();

    info!("Initializing sensors.");
    let sensor1 = if hostname == "forgacs1" {
        Sensor::new_pullup_invert(&gpio, 7)
    } else if hostname == "forgacs3" {
        Sensor::new_pulldown(&gpio, 6)
    } else {
        Sensor::fake()
    };
    let sensor2 = if hostname == "forgacs2" {
        Sensor::new_pullup(&gpio, 8)
    } else {
        Sensor::fake()
    };

    info!("Initializing servos.");
    let servo1 = servo::Servo::new(Channel::Pwm0);

    info!("Initializing switches.");
    let mut switch1 = Switch::new(&gpio, 15);
    let mut switch2 = Switch::new(&gpio, 16);

    info!("Initializing steppers.");

    let stepper1 = if hostname == "forgacs3" {
        Stepper::new_linear(
            Switch::new(&gpio, 21),
            Switch::new(&gpio, 20),
            Sensor::new_pullup_invert(&gpio, 2),
            Sensor::new_pullup_invert(&gpio, 3),
        )
    } else {
        let r = Stepper::new_linear(
            Switch::new(&gpio, 21),
            Switch::new(&gpio, 20),
            Sensor::fake(),
            Sensor::fake(),
        );
        r.tx.send(stepper::Msg::Fake()).unwrap();
        r
    };
    let stepper2 = if hostname == "forgacs1" {
        Stepper::new_circular(
            Switch::new(&gpio, 10),
            Switch::new(&gpio, 11),
            Sensor::new_pullup(&gpio, 4),
        )
    } else {
        let r = Stepper::new_circular(
            Switch::new(&gpio, 10),
            Switch::new(&gpio, 11),
            Sensor::fake(),
        );
        r.tx.send(stepper::Msg::Fake()).unwrap();
        r
    };

    info!("Initializing video.");
    cmds::stop();
    thread::sleep(Duration::from_secs(1));
    let video1 = Video::new(String::from("/home/pi/video"));

    info!("Initializing state.");
    let mut state = data::State::new();
    state.hostname = String::from(hostname);
    state.ip = String::from(ip);

    //
    // loop
    //
    info!("Initialized. Starting loop.");

    let (tx_msg, rx_msg) = mpsc::channel();
    tx_msg.send(Msg::ToStage(Stage::Init())).unwrap();

    let mut now = SystemTime::now();

    let mut play1_start_time = now;
    let mut button_press_time = now;
    //let mut button_release_time = now;

    loop {
        now = SystemTime::now();
        state.time = now.duration_since(UNIX_EPOCH).unwrap().as_millis() as u64;

        // get websocket msg
        for msg in web.rx.try_iter() {
            tx_msg.send(Msg::ControlMsg(msg)).unwrap();
        }

        // get msg
        for msg in rx_msg.try_iter() {
            info!("got msg: {:?}", msg);
            match msg {
                Msg::ToStage(stage) => match stage {
                    Stage::Init() => {
                        state.stage = Stage::Init();
                        state.started = false;
                        video1.tx_stop.send(()).unwrap();
                        stepper1.tx.send(stepper::Msg::MoveDir(None)).unwrap();
                        stepper2.tx.send(stepper::Msg::MoveDir(None)).unwrap();
                    }
                    Stage::Calibrate() => {
                        if state.stage == Stage::Init() || state.stage == Stage::Ready() {
                            state.stage = Stage::Calibrate();
                            stepper1.tx.send(stepper::Msg::Calibrate()).unwrap();
                            stepper2.tx.send(stepper::Msg::Calibrate()).unwrap();
                        }
                    }
                    Stage::Reset() => {
                        if state.stage == Stage::Init()
                            || state.stage == Stage::Calibrate()
                            || state.stage == Stage::Ready()
                            || state.stage == Stage::Play1()
                            || state.stage == Stage::Play2()
                        {
                            if state.is_calibrated() {
                                state.stage = Stage::Reset();

                                video1.tx_stop.send(()).unwrap();

                                stepper2.tx.send(stepper::Msg::Reset()).unwrap();

                                stepper1
                                    .tx
                                    .send(stepper::Msg::MoveAbs(PERF.stepper1_start))
                                    .unwrap();
                                stepper2
                                    .tx
                                    .send(stepper::Msg::MoveAbs(PERF.stepper2_start))
                                    .unwrap();
                                let m = servo::Msg::GoTo(PERF.servo1_start, Duration::from_secs(1));
                                servo1.tx.send(m).unwrap();
                            } else {
                                tx_msg.send(Msg::ToStage(Stage::Init())).unwrap();
                            }
                        }
                    }
                    Stage::Ready() => {
                        if state.is_calibrated() && state.is_reset() {
                            state.stage = Stage::Ready()
                        }
                    }
                    Stage::Play1() => {
                        if (state.stage == Stage::Ready() && state.is_reset())
                            || state.stage == Stage::Play2()
                        {
                            state.stage = Stage::Play1();
                            state.started = true;
                            play1_start_time = now;

                            state.switch1 = switch1.update(true);

                            video1.tx_start.send(()).unwrap();

                            stepper1.tx.send(stepper::Msg::MoveDir(None)).unwrap();
                            let len = (state.stepper1.length
                                - PERF.stepper1_end
                                - PERF.stepper1_start) as u64;
                            let period_pre =
                                (((video1.duration.as_millis() / 2 - 1000) * 1000) as u64) / len;
                            let atime = stepper::ramp_time(period_pre, stepper::LIN_ACCEL);
                            let stepper1_period =
                                ((((video1.duration.as_millis() / 2 - 1000) * 1000) as u64)
                                    - atime
                                    - atime / 100)
                                    / len;

                            let m = stepper::Msg::Period(stepper1_period);
                            stepper1.tx.send(m).unwrap();
                            let m =
                                stepper::Msg::MoveAbs(state.stepper1.length - PERF.stepper1_end);
                            stepper1.tx.send(m).unwrap();

                            let d = PERF.stepper2_start - state.stepper2.position;

                            info!("stepper2 distance to start: {:?}", d);
                            let stepper2_period = (video1.duration.as_micros() as f64
                                / (state.stepper2.length + d) as f64)
                                as u64;
                            let m = stepper::Msg::Period(stepper2_period);
                            stepper2.tx.send(m).unwrap();
                            let m = stepper::Msg::MoveDir(Some(true));
                            stepper2.tx.send(m).unwrap();

                            let m = servo::Msg::GoTo(
                                PERF.servo1_end,
                                Duration::from_micros((video1.duration.as_micros() / 2) as u64),
                            );
                            servo1.tx.send(m).unwrap();
                        }
                    }
                    Stage::Play2() => {
                        if state.stage == Stage::Play1() {
                            state.stage = Stage::Play2();

                            state.switch1 = switch1.update(false);

                            stepper1.tx.send(stepper::Msg::MoveDir(None)).unwrap();
                            let m = stepper::Msg::MoveAbs(PERF.stepper1_start);
                            stepper1.tx.send(m).unwrap();

                            let m = servo::Msg::GoTo(
                                PERF.servo1_start,
                                Duration::from_micros((video1.duration.as_micros() / 2) as u64),
                            );
                            servo1.tx.send(m).unwrap();
                        }
                    }
                },

                Msg::ControlMsg(control_msg) => match control_msg {
                    ControlMsg::Init() => tx_msg.send(Msg::ToStage(Stage::Init())).unwrap(),
                    ControlMsg::Calibrate() => {
                        tx_msg.send(Msg::ToStage(Stage::Calibrate())).unwrap()
                    }
                    ControlMsg::Reset() => tx_msg.send(Msg::ToStage(Stage::Reset())).unwrap(),
                    ControlMsg::Play1() => {
                        state.started = true;
                        tx_msg.send(Msg::ToStage(Stage::Play1())).unwrap()
                    }
                    ControlMsg::Play2() => tx_msg.send(Msg::ToStage(Stage::Play2())).unwrap(),
                    ControlMsg::Servo(i, v) => {
                        if i == 1 {
                            let msg = servo::Msg::GoTo(v, Duration::from_millis(300));
                            servo1.tx.send(msg).unwrap();
                        }
                    }
                    ControlMsg::Switch(i, v) => {
                        if i == 1 {
                            state.switch1 = switch1.update(v);
                        } else if i == 2 {
                            state.switch2 = switch2.update(v);
                        }
                    }
                    ControlMsg::StepperMsg(i, v) => {
                        if i == 1 {
                            stepper1.tx.send(v).unwrap();
                        } else if i == 2 {
                            stepper2.tx.send(v).unwrap();
                        }
                    }
                    ControlMsg::VideoPlay(i) => {
                        if i == 1 {
                            video1.tx_start.send(()).unwrap();
                        }
                    }
                    ControlMsg::VideoStop(i) => {
                        if i == 1 {
                            video1.tx_stop.send(()).unwrap();
                        }
                    }
                },
            }
        }

        for msg in stepper1.rx.try_iter() {
            state.stepper1 = msg;
        }

        for msg in stepper2.rx.try_iter() {
            state.stepper2 = msg;
        }

        for msg in servo1.rx.try_iter() {
            state.servo1 = msg;
        }

        for msg in video1.rx_state.try_iter() {
            state.video1 = msg;
        }

        for _msg in video1.rx_looped.try_iter() {
            info!("video looped");
        }

        for msg in sensor1.rx.try_iter() {
            let old_state = state.sensor1.state;
            state.sensor1.state = msg;
            info!("sensor1 new state: {:?}", msg);

            if msg {
                button_press_time = now;
            } else {
                if msg != old_state {
                    if !state.started {
                        if now.duration_since(button_press_time).unwrap().as_millis() >= 1000 {
                            tx_msg.send(Msg::ToStage(Stage::Reset())).unwrap();
                        } else {
                            state.started = true;
                            info!("state.started <- true");
                            tx_msg.send(Msg::ToStage(Stage::Play1())).unwrap()
                        }
                    } else {
                        if now.duration_since(button_press_time).unwrap().as_millis() >= 1000 {
                            state.started = false;
                            info!("state.started <- false");
                            tx_msg.send(Msg::ToStage(Stage::Reset())).unwrap();
                        } else {
                            tx_msg.send(Msg::ToStage(Stage::Init())).unwrap();
                        }
                    }
                }
            }
        }

        for msg in sensor2.rx.try_iter() {
            state.sensor2.state = msg;
            info!("sensor2 new state: {:?}", msg);
            if msg {
                video1.tx_start.send(()).unwrap();
            }
        }

        //
        // advance stages
        //
        match state.stage {
            Stage::Init() => {
                if state.started {
                    tx_msg.send(Msg::ToStage(Stage::Calibrate())).unwrap();
                }
            }
            Stage::Calibrate() => {
                if state.is_calibrated() {
                    tx_msg.send(Msg::ToStage(Stage::Reset())).unwrap();
                }
            }
            Stage::Reset() => {
                if state.is_reset() {
                    tx_msg.send(Msg::ToStage(Stage::Ready())).unwrap();
                }
            }
            Stage::Ready() => {
                if state.started {
                    if state.is_reset() {
                        tx_msg.send(Msg::ToStage(Stage::Play1())).unwrap();
                    }
                }
            }
            Stage::Play1() => {
                if now.duration_since(play1_start_time).unwrap() >= video1.duration / 2 {
                    tx_msg.send(Msg::ToStage(Stage::Play2())).unwrap();
                }
            }
            Stage::Play2() => {
                if now.duration_since(play1_start_time).unwrap() >= video1.duration {
                    if state.started {
                        tx_msg.send(Msg::ToStage(Stage::Play1())).unwrap();
                    } else {
                        tx_msg.send(Msg::ToStage(Stage::Reset())).unwrap();
                    }
                }
            }
        }

        //
        // output
        //

        // output websocket
        let state_str = serde_json::to_string(&state).unwrap();
        web.tx.send(state_str).unwrap();

        // delay
        thread::sleep(Duration::from_millis(10));
    }

    //info!("end reached");
}
