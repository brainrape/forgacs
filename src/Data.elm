module Data exposing (..)

import Json.Decode as D
import Json.Encode as E


fi : Bool -> a -> a -> a
fi p t f =
    if p then
        t

    else
        f


type alias State =
    { hostname : String
    , ip : String
    , time : Int
    , stage : Stage
    , sensor1 : SensorState
    , sensor2 : SensorState
    , servo1 : ServoState
    , switch1 : SwitchState
    , switch2 : SwitchState
    , stepper1 : StepperState
    , stepper2 : StepperState
    , video1 : VideoState
    }


type Stage
    = Init
    | Calibrate
    | Reset
    | Ready
    | Play1
    | Play2


initState : State
initState =
    { hostname = ""
    , ip = ""
    , time = 0
    , stage = Init
    , sensor1 = SensorState False
    , sensor2 = SensorState False
    , servo1 = ServoState 0
    , switch1 = SwitchState False
    , switch2 = SwitchState False
    , stepper1 = initStepperState
    , stepper2 = initStepperState
    , video1 = VideoState False 0
    }


type alias StepperState =
    { calibrating : Bool
    , calibrated : Bool
    , exists : Bool
    , period : Int
    , length : Int
    , position : Int
    , target : Int
    , dir : Maybe Bool
    , sensor1 : Bool
    , sensor2 : Bool
    }


initStepperState : StepperState
initStepperState =
    { calibrating = False
    , calibrated = False
    , exists = True
    , period = 0
    , length = 0
    , position = 0
    , target = 0
    , dir = Nothing
    , sensor1 = False
    , sensor2 = False
    }


type alias ServoState =
    { position : Float
    }


type alias SensorState =
    { state : Bool }


type alias SwitchState =
    { state : Bool }


type alias VideoState =
    { playing : Bool, duration : Int }


type alias Id =
    Int


type alias MotorState =
    Maybe Bool


type ControlMsg
    = GoInit
    | GoCalibrate
    | GoReset
    | GoPlay
    | Servo Id Float
    | Switch Id Bool
    | Motor Id MotorState
    | StepperMoveRel Id Int
    | StepperPeriod Id Int
    | StepperMoveDir Id MotorState
    | StepperCalibrate Id
    | StepperPlay Id
    | VideoPlay Id
    | VideoStop Id


decodeApply : D.Decoder (a -> b) -> D.Decoder a -> D.Decoder b
decodeApply df dv =
    D.map2 (|>) dv df


required : String -> D.Decoder a -> D.Decoder (a -> b) -> D.Decoder b
required fieldName itemDecoder functionDecoder =
    decodeApply functionDecoder (D.field fieldName itemDecoder)


decodeState : D.Decoder State
decodeState =
    D.succeed State
        |> required "hostname" D.string
        |> required "ip" D.string
        |> required "time" D.int
        |> required "stage" decodeStage
        |> required "sensor1" decodeSensorState
        |> required "sensor2" decodeSensorState
        |> required "servo1" decodeServoState
        |> required "switch1" decodeSwitchState
        |> required "switch2" decodeSwitchState
        |> required "stepper1" decodeStepperState
        |> required "stepper2" decodeStepperState
        |> required "video1" decodeVideoState


decodeStage : D.Decoder Stage
decodeStage =
    D.oneOf
        [ D.succeed (\_ -> Init) |> required "Init" (D.succeed ())
        , D.succeed (\_ -> Calibrate) |> required "Calibrate" (D.succeed ())
        , D.succeed (\_ -> Reset) |> required "Reset" (D.succeed ())
        , D.succeed (\_ -> Ready) |> required "Ready" (D.succeed ())
        , D.succeed (\_ -> Play1) |> required "Play1" (D.succeed ())
        , D.succeed (\_ -> Play2) |> required "Play2" (D.succeed ())
        ]


encodeControlMsg : ControlMsg -> E.Value
encodeControlMsg msg =
    E.string (controlMsgToString msg)


controlMsgToString : ControlMsg -> String
controlMsgToString msg =
    case msg of
        _ ->
            Debug.toString msg


decodeStepperState : D.Decoder StepperState
decodeStepperState =
    D.succeed StepperState
        |> required "calibrating" D.bool
        |> required "calibrated" D.bool
        |> required "exists" D.bool
        |> required "period" D.int
        |> required "length" D.int
        |> required "position" D.int
        |> required "target" D.int
        |> required "dir" (D.maybe D.bool)
        |> required "sensor1" D.bool
        |> required "sensor2" D.bool


decodeSwitchState : D.Decoder SwitchState
decodeSwitchState =
    D.succeed SwitchState |> required "state" D.bool


decodeSensorState : D.Decoder SensorState
decodeSensorState =
    D.succeed SensorState |> required "state" D.bool


decodeServoState : D.Decoder ServoState
decodeServoState =
    D.succeed ServoState |> required "position" D.float


decodeVideoState : D.Decoder VideoState
decodeVideoState =
    D.succeed VideoState
        |> required "playing" D.bool
        |> required "duration" (D.map2 (\s n -> s * 1000 + n // 1000000) (D.field "secs" D.int) (D.field "nanos" D.int))
