use std::time::Duration;

pub struct Perf {
    pub play_duration: Duration,
    pub stepper1_start: i64,
    pub stepper1_end: i64,
    pub stepper2_start: i64,
    pub servo1_start: f64,
    pub servo1_end: f64,
}

pub const PERF: Perf = Perf {
    play_duration: Duration::from_secs(60),
    stepper1_start: 1000,
    stepper1_end: 1000,
    stepper2_start: 26540,
    servo1_start: 0.3,
    servo1_end: 0.7,
};
