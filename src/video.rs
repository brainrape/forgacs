extern crate core_affinity;
use serde::{Deserialize, Serialize};
use std::io::Read;
//use std::io::Write;
use std::process::{Command, Stdio};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::time::Duration;
use tracing::{info, warn};

use super::cmds;

pub struct Video {
    pub tx_start: Sender<()>,
    pub tx_stop: Sender<()>,
    pub rx_state: Receiver<State>,
    pub rx_looped: Receiver<()>,
    pub filename: String,
    pub duration: Duration,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct State {
    pub playing: bool,
    pub duration: Duration,
}

impl State {
    pub fn new() -> State {
        State {
            playing: false,
            duration: Duration::from_secs(60),
        }
    }
}

impl Video {
    pub fn new(filename: String) -> Video {
        info!("Initializing video...");
        let (tx_start, rx_start) = mpsc::channel();
        let (tx_stop, rx_stop) = mpsc::channel();
        let (tx_model, rx_state) = mpsc::channel();
        let (tx_looped, rx_looped) = mpsc::channel();

        let duration;
        let filename_clone = filename.clone();
        if let Some(duration_) = cmds::get_video_duration(filename_clone.as_str()) {
            info!("Got video duration: {:?}.", duration_.as_secs());
            if duration_.as_secs() < 30 {
                warn!("Video duration too short. Setting to 60 sec.");
                duration = Duration::from_secs(60);
            } else {
                duration = duration_;
            }
        } else {
            warn!("Could not get video duration. Setting to 60 sec.");
            duration = Duration::from_secs(60);
        }

        thread::spawn(move || loop {
            core_affinity::set_for_current(core_affinity::CoreId { id: 1 });
            if let Ok(()) = rx_start.recv() {
                info!("got first start msg");
                tx_model
                    .send(State {
                        playing: true,
                        duration: duration,
                    })
                    .unwrap();
                for _ in rx_stop.try_iter() {}
            } else {
                thread::sleep(Duration::from_secs(1));
                continue;
            };

            let mut cmd = Command::new("omxplayer")
                .arg(filename_clone.clone())
                .arg("--loop")
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()
                .unwrap();

            //let mut cmd_in = cmd.stdin.take().unwrap();
            let mut cmd_out = cmd.stdout.take().unwrap();

            let tx_looped_clone = tx_looped.clone();

            let (tx_cmd_stopped, rx_cmd_stopped) = mpsc::channel();
            thread::spawn(move || {
                loop {
                    core_affinity::set_for_current(core_affinity::CoreId { id: 1 });
                    info!("cmd thread looping");

                    let mut buf: [u8; 255] = [0; 255];
                    let mut buf1: [u8; 1] = [0];
                    let mut i = 0;
                    loop {
                        let r = cmd_out.read(&mut buf1);
                        if let Ok(1) = r {
                            if buf1 == ([b'\n']) {
                                break;
                            }
                            buf[i] = buf1[0];
                            i = i + 1;
                            if i > 255 {
                                i = 0;
                            }
                        }
                    }

                    info!("i {:?}", i);
                    let output = std::str::from_utf8(&buf[0..i])
                        .unwrap()
                        .trim_matches('\x00');

                    // let r = cmd_out.read(&mut buf).unwrap();
                    // info!("r {:?}", r);

                    // let output = std::str::from_utf8(&buf).unwrap().trim_matches('\x00');

                    if output.len() > 0 {
                        info!("out: {:?}", output);
                    }

                    if output == "Seek to: 00:00:00" {
                        tx_looped_clone.send(()).unwrap();
                    }

                    if let Ok(()) = rx_cmd_stopped.try_recv() {
                        info!("cmd stopped msg");
                        break;
                    }

                    std::thread::sleep(Duration::from_millis(100));
                }
            });

            loop {
                //info!("looping");

                if let Ok(()) = rx_start.try_recv() {
                    info!("got start msg");
                    // cmd_in.write(b"x1B[A").unwrap();
                    // // ESC [ A
                    Command::new("bash")
                        .args(&["-c", "/home/pi/forgacs/dbuscontrol.sh seek -100000000"])
                        .output()
                        .unwrap();
                }

                if let Ok(()) = rx_stop.try_recv() {
                    info!("got stop msg");
                    //cmd.kill().unwrap();
                    Command::new("bash")
                        .args(&["-c", "/home/pi/forgacs/dbuscontrol.sh stop"])
                        .output()
                        .unwrap();
                }

                let rw = cmd.try_wait();
                //info!("rw {:?}", rw);
                if let Ok(Some(status)) = rw {
                    info!("player process exited: {:?}", status);
                    tx_cmd_stopped.send(()).unwrap();
                    tx_model
                        .send(State {
                            playing: false,
                            duration: duration,
                        })
                        .unwrap();
                    break;
                } else {
                    thread::sleep(Duration::from_millis(100))
                }
            }
            cmd.wait_with_output().unwrap();
        });
        return Video {
            tx_start: tx_start,
            tx_stop: tx_stop,
            rx_state: rx_state,
            rx_looped: rx_looped,
            filename: filename,
            duration: duration,
        };
    }
}
