module Main exposing (main)

import Browser exposing (element)
import Data exposing (..)
import Page exposing (Model, Msg(..), initModel, update)
import Ports
import Time exposing (Posix, every)
import View exposing (view)


init : () -> ( Model, Cmd Msg )
init _ =
    ( initModel, Cmd.none )


main : Program () Model Msg
main =
    element
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.batch [ Ports.recv Recv, every 2000 Tick ]
        }
