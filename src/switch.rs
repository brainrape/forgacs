use rppal::gpio::{Gpio, OutputPin};
use serde::{Deserialize, Serialize};

pub struct Switch {
    pub pin: OutputPin,
    pub pin_number: u8,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct State {
    state: bool,
}

impl State {
    pub fn new() -> State {
        State { state: false }
    }
}

impl Switch {
    pub fn new(gpio: &Gpio, pin_number: u8) -> Switch {
        let pin = gpio.get(pin_number).unwrap().into_output();
        return Switch {
            pin: pin,
            pin_number: pin_number,
        };
    }

    pub fn update(&mut self, val: bool) -> State {
        if val {
            self.pin.set_high();
        } else {
            self.pin.set_low();
        }
        return State { state: val };
    }
}
