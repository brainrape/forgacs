extern crate core_affinity;
use super::cmds;
use rppal::gpio::Gpio;
use serde::{Deserialize, Serialize};
use std::sync::mpsc;
use std::sync::mpsc::Receiver;
use std::thread;
use std::time::Duration;

pub struct Sensor {
    pub pin_number: u8,
    pub pullup: bool,
    pub invert: bool,
    pub rx: Receiver<bool>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct State {
    pub state: bool,
}

impl State {
    pub fn new() -> State {
        State { state: false }
    }
}

#[allow(dead_code)]
impl Sensor {
    pub fn new_pullup(gpio: &Gpio, pin_number: u8) -> Sensor {
        Sensor::new(gpio, pin_number, true, false)
    }

    pub fn new_pullup_invert(gpio: &Gpio, pin_number: u8) -> Sensor {
        Sensor::new(gpio, pin_number, true, true)
    }

    pub fn new_pulldown(gpio: &Gpio, pin_number: u8) -> Sensor {
        Sensor::new(gpio, pin_number, false, false)
    }

    pub fn new_pulldown_invert(gpio: &Gpio, pin_number: u8) -> Sensor {
        Sensor::new(gpio, pin_number, false, true)
    }

    pub fn fake() -> Sensor {
        let (tx, rx) = mpsc::channel();
        tx.send(false).unwrap();
        return Sensor {
            pin_number: 0,
            pullup: false,
            invert: false,
            rx: rx,
        };
    }
    pub fn new(gpio: &Gpio, pin_number: u8, pullup: bool, invert: bool) -> Sensor {
        let pin;
        if pullup {
            cmds::set_pullup(pin_number);
            pin = gpio.get(pin_number).unwrap().into_input_pullup()
        } else {
            cmds::set_pulldown(pin_number);
            pin = gpio.get(pin_number).unwrap().into_input_pulldown()
        }

        let (tx, rx) = mpsc::channel();

        thread::spawn(move || {
            core_affinity::set_for_current(core_affinity::CoreId { id: 2 });

            let mut state = false;
            let mut found = 0;

            loop {
                let sample = if !invert { pin.is_high() } else { pin.is_low() };

                if sample != state {
                    found += 1;
                } else {
                    found = 0;
                };
                if found > 10 {
                    found = 0;
                    state = sample;
                    tx.send(state).unwrap();
                }
                thread::sleep(Duration::from_micros(400));
            }
        });

        Sensor {
            pin_number: pin_number,
            pullup: pullup,
            invert: invert,
            rx: rx,
        }
    }
}
