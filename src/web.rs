extern crate core_affinity;
use super::data;
use iron::Iron;
use mount::Mount;
use staticfile::Static;
use std::path::Path;
use std::sync::mpsc;
use std::thread;
use tracing::{info, warn};

pub struct Web {
    pub rx: mpsc::Receiver<data::ControlMsg>,
    pub tx: ws::Sender,
}

impl Web {
    pub fn new() -> Web {
        info!("Starting web server on on http://0.0.0.0:2220/");
        let mut mount = Mount::new();
        mount.mount("/x5q6rzvd/", Static::new(Path::new("out/")));
        let _web = thread::spawn(move || {
            core_affinity::set_for_current(core_affinity::CoreId { id: 1 });

            Iron::new(mount).http("0.0.0.0:2220").unwrap();
        });

        info!("Starting websocket server");
        let (ws_in_tx, ws_in_rx) = mpsc::channel();
        let socket = ws::Builder::new()
            .with_settings(ws::Settings {
                tcp_nodelay: true,
                ..ws::Settings::default()
            })
            .build(move |_out| {
                let tx_ = ws_in_tx.clone();
                move |ws_msg: ws::Message| {
                    if let Ok(msg_txt) = ws_msg.as_text() {
                        info!("got msg: {:?}", msg_txt);
                        if let Some(msg) = data::decode(msg_txt) {
                            tx_.send(msg).unwrap();
                        }
                    } else {
                        warn!("got bad websocket msg: {:?}", ws_msg)
                    };
                    Ok(())
                }
            })
            .unwrap();
        let ws_out_tx = socket.broadcaster();
        let _websocket_thread = thread::spawn(move || {
            core_affinity::set_for_current(core_affinity::CoreId { id: 1 });

            socket.listen("0.0.0.0:2221").unwrap()
        });
        return Web {
            rx: ws_in_rx,
            tx: ws_out_tx,
        };
    }
}
