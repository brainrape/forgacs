port module Ports exposing (..)

import Json.Encode exposing (Value)


port send : String -> Cmd msg


port recv : (Maybe String -> msg) -> Sub msg
