extern crate core_affinity;
use super::sensor::Sensor;
use super::switch::Switch;
use serde::{Deserialize, Serialize};
use spin_sleep;
//use std::cmp::max;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::time::Duration;
use tracing::{info, warn};

pub struct Stepper {
    pub rx: Receiver<State>,
    pub tx: Sender<Msg>,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum Msg {
    Calibrate(),
    Reset(),
    Period(u64),
    MoveDir(Option<bool>),
    MoveAbs(i64),
    Fake(),
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub struct State {
    pub calibrating: bool,
    pub calibrated: bool,
    pub exists: bool,
    pub period: u64,
    pub length: i64,
    pub position: i64,
    pub target: i64,
    pub dir: Option<bool>,
    pub sensor1: bool,
    pub sensor2: bool,
}

impl State {
    pub fn new() -> State {
        State {
            calibrating: false,
            calibrated: false,
            exists: true,
            period: 800,
            length: 0,
            position: 0,
            target: 0,
            dir: None,
            sensor1: false,
            sensor2: false,
        }
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq)]
pub enum MState {
    Uncalibrated(),
    Dummy(),
    Calibrating(bool),
    Stopped(),
    Starting(u64),
    Moving(),
    Stopping(u64),
}

const BILLIONTH: f64 = 1.0 / 1000000000.0;
const MIN_PERIOD: u64 = 30;
const SMALL_STEPS: u64 = 40;
const SAFETY: u64 = 500;
const BIG_PAUSE: Option<Duration> = Some(Duration::from_secs(1));
const DELAY_ADJUSTMENT: u64 = 3;

const LIN_CALIBRATION_PERIOD: u64 = 50;
const LIN_CALIBRATION_STOP_STEPS: u64 = 1600;
const LIN_STOP_PERIOD: u64 = 4000;
pub const LIN_ACCEL: f64 = 20.0 * BILLIONTH;

const ROT_CALIBRATION_PERIOD: u64 = 1000;
const ROT_STOP_PERIOD: u64 = 10000;
pub const ROT_ACCEL: f64 = 2.0 * BILLIONTH;

fn f_starting(i: u64, steps: u64, period: u64, stop_period: u64) -> u64 {
    if i > steps {
        period
    } else {
        let f = (1.0 - ((i as f64) / (steps as f64))).powf(3.0);
        ((period as f64 * (1.0 - f)) + (stop_period as f64 * f)) as u64
    }
}

fn f_stopping(i: u64, steps: u64, period: u64, stop_period: u64) -> u64 {
    if i > steps {
        stop_period
    } else {
        let f = (1.0 - (i as f64 / steps as f64)).powf(3.0);
        ((period as f64 * (1.0 - f)) + (stop_period as f64 * f)) as u64
    }
}

pub fn accel_steps(period: u64, acceleration: f64) -> u64 {
    (1.0 / (2.0 * acceleration * ((period as f64).powf(2.0)))) as u64
}

pub fn steps_accel(period: u64, steps: u64) -> f64 {
    1.0 / (2.0 * (steps as f64) * (period as f64).powf(2.0))
}

fn mkv(from_: u64, to_: u64, acceleration: f64) -> Vec<u64> {
    let c0: f64 = (2.0 / acceleration).sqrt();
    // info!("c0 {:?}", c0);
    let mut v: Vec<u64> = Vec::new();
    let (from, to) = if to_ > from_ {
        (from_, to_)
    } else {
        (to_, from_)
    };
    for i in from..to {
        if i == 0 {
            v.push(c0 as u64);
        } else {
            let x = (c0 * (((i + 1) as f64).sqrt() - (i as f64).sqrt())) as u64;
            v.push(x);
        }
    }
    if to_ > from_ {
        v
    } else {
        v.reverse();
        v
    }
}

pub fn ramp_time(period: u64, acceleration: f64) -> u64 {
    let asteps = accel_steps(period, acceleration);
    let sum: u64 = mkv(0, asteps, acceleration).iter().sum();
    sum - asteps * period
}

fn dirof(i: i64) -> i64 {
    if i > 0 {
        1
    } else if i < 0 {
        -1
    } else {
        0
    }
}

fn handle_msg_def(
    msg: Msg,
    _length: i64,
    period: u64,
    target: i64,
    position: i64,
    mstate: MState,
) -> (u64, i64, i64, MState) {
    match msg {
        Msg::Period(micros) => {
            if micros < MIN_PERIOD {
                warn!("period too small: {:?}", micros);
                (MIN_PERIOD, target, position, mstate)
            } else {
                (micros, target, position, mstate)
            }
        }
        Msg::Fake() => {
            info!("stepper is fake dummy");
            (period, 0, 0, MState::Dummy())
        }
        _ => (period, target, position, mstate),
    }
}

fn handle_msg_lin(
    msg: Msg,
    length: i64,
    period: u64,
    asteps: u64,
    target: i64,
    position: i64,
    mstate: MState,
) -> (u64, i64, i64, MState) {
    let nop = (period, target, position, mstate);
    match msg {
        Msg::Calibrate() => {
            if mstate == MState::Uncalibrated() {
                (LIN_CALIBRATION_PERIOD, target, 0, MState::Calibrating(true))
            } else {
                nop
            }
        }
        Msg::MoveAbs(x) => {
            if x < (SAFETY as i64) || x > length - (SAFETY as i64) {
                warn!("stepper1 MoveAbs out of bounds: {:?}", x);
                warn!(
                    "{:?}",
                    (
                        x,
                        SAFETY,
                        SAFETY as i64,
                        x < (SAFETY as i64),
                        length,
                        length - (SAFETY as i64),
                        x > length - (SAFETY as i64)
                    )
                );
                nop
            } else {
                match mstate {
                    MState::Stopped() => (period, x, position, MState::Starting(0)),
                    MState::Starting(step) => {
                        if (x - position) * (target - position) >= 0 {
                            (period, x, position, MState::Starting(step))
                        } else {
                            (period, target, position, MState::Stopping(step))
                        }
                    }
                    MState::Moving() => {
                        if (x - position) * (target - position) > 0
                            && (x - position).abs() > (asteps as i64)
                        {
                            (period, x, position, mstate)
                        } else {
                            let p = position + dirof(target - position) * (asteps as i64);
                            (period, p, position, MState::Stopping(asteps))
                        }
                    }
                    MState::Stopping(step) => {
                        if (x - position) * (target - position) > 0 {
                            if (x - position).abs() > (asteps as i64) {
                                (period, x, position, MState::Starting(step))
                            } else {
                                let t = position + dirof(target - position) * (asteps as i64);
                                (period, t, position, mstate)
                            }
                        } else {
                            (period, x, position, mstate)
                        }
                    }
                    _ => nop,
                }
            }
        }
        Msg::MoveDir(Some(fwd)) => {
            if mstate == MState::Stopped() {
                let t = if fwd {
                    length - (SAFETY as i64)
                } else {
                    SAFETY as i64
                };
                (period, t, position, MState::Starting(0))
            } else {
                nop
            }
        }
        Msg::MoveDir(None) => match mstate {
            MState::Calibrating(_) => (period, target, position, MState::Uncalibrated()),
            MState::Starting(step) => (period, target, position, MState::Stopping(step)),
            MState::Moving() => (
                period,
                position + dirof(target - position) * (asteps as i64),
                position,
                MState::Stopping(asteps as u64),
            ),
            _ => nop,
        },
        Msg::Reset() => nop,
        _ => handle_msg_def(msg, length, period, target, position, mstate),
    }
}

// result: (length, target,position, mstate, setdir, periods, wait = wait_)
fn update_starting(
    step: u64,
    stop_period: u64,
    accel: f64,
    length: i64,
    period: u64,
    asteps: u64,
    target: i64,
    position: i64,
    _mstate: MState,
) -> (
    i64,
    i64,
    i64,
    MState,
    Option<bool>,
    Vec<u64>,
    Option<Duration>,
) {
    let mut v: Vec<u64> = Vec::new();
    let d = target - position;
    // info!("update_starting: {:?}", (step, stop_period, accel, length, period, asteps, target, position, mstate, d));
    let setdir = if step == 0 { Some(d >= 0) } else { None };
    if (d.abs() as u64) <= step {
        info!("update_starting: target is close; Stopping");
        (
            length,
            target,
            position,
            MState::Stopping(step),
            setdir,
            v,
            None,
        )
    } else if (d.abs() as u64) < asteps {
        if d.abs() as u64 > SMALL_STEPS {
            info!("update_starting: target is closer than stop distance; slow down; Stopped");
        } else {
            info!("update_starting: target is very close; slow down; Stopped");
        }
        let p = f_starting(step, asteps, period, stop_period);
        for i in 0..(d.abs() as u64) {
            v.push(f_stopping(i, d.abs() as u64, p, stop_period));
        }
        (length, target, position, MState::Stopped(), setdir, v, None)
    } else if step < asteps {
        let dx = std::cmp::min(asteps - step, SMALL_STEPS);
        let m = MState::Starting(step + dx);

        let v = mkv(step, step + dx, accel);
        // info!("Starting {:?}", v.to_vec());
        (length, target, position, m, setdir, v, None)
    } else {
        // starting done, moving.
        info!("update_starting: starting done, switching to Moving.");
        (
            length,
            target,
            position,
            MState::Moving(),
            None,
            v.to_vec(),
            None,
        )
    }
}

// update state
// result: (length, target,position, mstate, setdir, periods, wait = wait_)
fn update_lin(
    length: i64,
    period: u64,
    asteps: u64,
    target: i64,
    position: i64,
    mstate: MState,
    s1: bool,
    s2: bool,
) -> (
    i64,
    i64,
    i64,
    MState,
    Option<bool>,
    Vec<u64>,
    Option<Duration>,
) {
    let sensors_ok_lin = |x| {
        if (target - position > 0 && s2) || (target - position < 0 && s1) {
            warn!("sensors triggered:  sensor1: {:?}  sensor2: {:?}", s1, s2);
            (
                length,
                position,
                position,
                MState::Stopped(),
                None,
                Vec::new(),
                BIG_PAUSE,
            )
        } else {
            x
        }
    };

    let mut v: Vec<u64> = Vec::new();
    let no_dir: Option<bool> = None;
    let no_dur: Option<Duration> = None;
    let set_mstate = |x: MState| (length, target, position, x, no_dir, v.to_vec(), no_dur);
    let set_periods = |ps: _| (length, target, position, mstate, no_dir, ps, no_dur);
    let old_mstate = mstate.clone();
    let calibration_accel_steps = steps_accel(LIN_CALIBRATION_PERIOD, LIN_CALIBRATION_STOP_STEPS);

    let nop = (length, target, position, mstate, None, v.to_vec(), None);
    match mstate {
        MState::Uncalibrated() => {
            thread::sleep(Duration::from_millis(10));
            nop
        }
        MState::Dummy() => {
            thread::sleep(Duration::from_millis(100));
            nop
        }
        MState::Calibrating(true) => {
            if s2 {
                info!("Calibrating found forward end");
                v = mkv(LIN_CALIBRATION_STOP_STEPS, 0, calibration_accel_steps);
                let s = MState::Calibrating(false);
                (10000000, v.len() as i64, 0, s, None, v, BIG_PAUSE)
            } else {
                if position >= 10000000 {
                    set_mstate(MState::Dummy())
                } else {
                    if position == 0 {
                        v = mkv(0, LIN_CALIBRATION_STOP_STEPS, calibration_accel_steps);
                    } else {
                        v = vec![LIN_CALIBRATION_PERIOD; SMALL_STEPS as usize]
                    }
                    let d = Some(true);
                    (length, target, position, mstate, d, v, None)
                }
            }
        }
        MState::Calibrating(false) => {
            if s1 {
                info!("Calibrating found backward end: {:?}", position);
                v = mkv(LIN_CALIBRATION_STOP_STEPS, 0, calibration_accel_steps);
                (position.abs(), 0, 0, MState::Stopped(), None, v, BIG_PAUSE)
            } else {
                if position < -10000000 {
                    set_mstate(MState::Dummy())
                } else {
                    if position == 0 {
                        v = mkv(0, LIN_CALIBRATION_STOP_STEPS, calibration_accel_steps);
                    } else {
                        v = vec![LIN_CALIBRATION_PERIOD; SMALL_STEPS as usize];
                    }
                    let d = Some(false);
                    (length, target, position, mstate, d, v, None)
                }
            }
        }
        MState::Stopped() => {
            if let MState::Calibrating(_) = old_mstate {
                let l = (LIN_CALIBRATION_STOP_STEPS + 1000) / 2;
                v.append(&mut mkv(0, l, steps_accel(LIN_CALIBRATION_PERIOD * 2, l)));
                v.append(&mut mkv(l, 0, steps_accel(LIN_CALIBRATION_PERIOD * 2, l)));
                let dir = Some(false);
                (length, target, position, mstate, dir, v, None)
            } else {
                if target == position {
                    let d = Some(Duration::from_millis(2));
                    (length, target, position, mstate, None, v, d)
                } else {
                    let s = MState::Starting(0);
                    (length, target, position, s, None, v, None)
                }
            }
        }
        MState::Starting(step) => sensors_ok_lin(update_starting(
            step,
            LIN_STOP_PERIOD,
            LIN_ACCEL,
            length,
            period,
            asteps,
            target,
            position,
            mstate,
        )),
        MState::Moving() => {
            let d = target - position;
            sensors_ok_lin(if d.abs() > ((asteps + SMALL_STEPS) as i64) {
                set_periods(vec![period; SMALL_STEPS as usize])
            } else if d.abs() > (asteps as i64) {
                set_periods(vec![period; (d.abs() - (asteps as i64)) as usize])
            } else {
                set_mstate(MState::Stopping(asteps))
            })
        }
        MState::Stopping(step) => {
            if step <= 0 {
                (length, target, position, MState::Stopped(), None, v, None)
            } else {
                let d = std::cmp::min(step, SMALL_STEPS);
                let v = mkv(step, step - d, LIN_ACCEL);
                let m = MState::Stopping(step - d);
                sensors_ok_lin((length, target, position, m, None, v, None))
            }
        }
    }
}

fn serialize_state(
    length: i64,
    period: u64,
    target: i64,
    position: i64,
    mstate: MState,
    s1: bool,
    s2: bool,
) -> State {
    let is_calibrating = match mstate {
        MState::Calibrating(_) => true,
        _ => false,
    };

    let is_calibrated = match mstate {
        MState::Uncalibrated() => false,
        MState::Calibrating(_) => false,
        _ => true,
    };

    let dir = if target > position {
        Some(true)
    } else if target < position {
        Some(false)
    } else {
        None
    };

    State {
        calibrating: is_calibrating,
        calibrated: is_calibrated,
        exists: mstate != MState::Dummy(),
        period: period,
        length: length,
        position: position,
        target: target,
        dir: dir,
        sensor1: s1,
        sensor2: s2,
    }
}

impl Stepper {
    pub fn new_linear(
        mut dir_sw: Switch,
        mut pul_sw: Switch,
        sensor1: Sensor,
        sensor2: Sensor,
    ) -> Stepper {
        let (tx_msg, rx_msg) = mpsc::channel();
        let (tx_model, rx_model) = mpsc::channel();

        thread::spawn(move || {
            core_affinity::set_for_current(core_affinity::CoreId { id: 3 });

            //let mut model = data::State::new();
            let mut position: i64 = 0;
            let mut target: i64 = 0;
            let mut period: u64 = LIN_CALIBRATION_PERIOD;
            let mut asteps = accel_steps(period, LIN_ACCEL);
            let mut length: i64 = 10000;
            let mut dir = false;
            let mut s1 = false;
            let mut s2 = false;

            let mut mstate = MState::Uncalibrated();

            loop {
                //handle messages
                let old_mstate = mstate.clone();
                if let Ok(msg) = rx_msg.try_recv() {
                    if mstate != MState::Dummy() {
                        info!("stepper1 thread got msg: {:?}", msg);
                    }
                    #[allow(irrefutable_let_patterns)]
                    if let (period_, target_, position_, mstate_) =
                        handle_msg_lin(msg, length, period, asteps, target, position, mstate)
                    {
                        if mstate != mstate_ {
                            info!("stepper1 msg new mstate: {:?}", mstate_);
                        }
                        period = period_;
                        asteps = accel_steps(period, LIN_ACCEL);
                        target = target_;
                        position = position_;
                        mstate = mstate_;
                    }
                }

                // get sensor values
                if let Ok(msg) = sensor1.rx.try_recv() {
                    s1 = msg;
                };
                if let Ok(msg) = sensor2.rx.try_recv() {
                    s2 = msg;
                };

                // update model
                let (mut setdir, mut wait) = (None, None);
                let mut periods: Vec<u64> = Vec::new();
                #[allow(irrefutable_let_patterns)]
                if let (length_, target_, position_, mstate_, setdir_, periods_, wait_) =
                    update_lin(length, period, asteps, target, position, mstate, s1, s2)
                {
                    if old_mstate != mstate_ {
                        let mut p1 = periods_.to_vec();
                        let mut p2 = periods_.to_vec();
                        p1.truncate(3);
                        p2.reverse();
                        p2.truncate(3);
                        p2.reverse();
                        info!(
                            "stepper1: changed MState to {:?} | length: {:?} | target: {:?}  | position: {:?} |  wait: {:?} | setdir: {:?} | {:?} .. {:?} ({:?})",
                            mstate_, length_, target_, position_, setdir_, wait_, p1, p2, periods_.len()
                        );
                    }
                    length = length_;
                    target = target_;
                    position = position_;
                    mstate = mstate_;
                    setdir = setdir_;
                    periods = periods_;
                    wait = wait_;
                }

                if false // debug
                    && mstate != MState::Uncalibrated()
                    && mstate != MState::Dummy()
                    && mstate != MState::Stopped()
                    && position % 10000 == 0
                    && mstate != MState::Calibrating(false)
                    && mstate != MState::Calibrating(true)
                {
                    let mut p1 = periods.to_vec();
                    let mut p2 = periods.to_vec();
                    p1.truncate(3);
                    p2.reverse();
                    p2.truncate(3);
                    p2.reverse();
                    info!(
                        "stepper1: MState: {:?} | length: {:?} | period: {:?} | target: {:?}  | position: {:?} | {:?} .. {:?} ({:?})",
                        mstate ,length, period, target, position, p1, p2, periods.len()
                    );
                }

                // output model
                let model = serialize_state(length, period, target, position, mstate, s1, s2);
                tx_model.send(model).unwrap();

                // set direction
                if let Some(fwd) = setdir {
                    dir = fwd;
                    dir_sw.update(fwd);
                    spin_sleep::sleep(Duration::from_micros(10));
                }

                // ouput pulses
                if periods.len() > 0 {
                    for i in 0..periods.len() {
                        let p = if periods[i] < MIN_PERIOD {
                            warn!("period too small: {:?}", periods[i]);
                            MIN_PERIOD
                        } else {
                            periods[i]
                        };

                        let dp = if dir { 1 } else { -1 };
                        if let MState::Calibrating(_) = mstate {
                        } else if let (MState::Calibrating(_), MState::Stopped()) =
                            (old_mstate, mstate)
                        {
                        } else {
                            if (dp > 0 && position + dp > length) || (dp < 0 && position + dp <= 0)
                            {
                                warn!("stepper1 reached limit!, position: ${:?}", position);
                                break;
                            }
                        }

                        // pul_sw.update(false);
                        pul_sw.update(true);
                        spin_sleep::sleep(Duration::from_micros(5));
                        pul_sw.update(false);
                        spin_sleep::sleep(Duration::from_micros(p - 5 - DELAY_ADJUSTMENT));
                        position += dp;
                    }
                }

                // wait
                if let Some(dur) = wait {
                    thread::sleep(dur);
                }
            } // loop
        });

        return Stepper {
            tx: tx_msg,
            rx: rx_model,
        };
    }
}

fn handle_msg_rot(
    msg: Msg,
    length: i64,
    period: u64,
    asteps: u64,
    target: i64,
    position: i64,
    mstate: MState,
) -> (u64, i64, i64, MState) {
    let nop = (period, target, position, mstate);
    match msg {
        Msg::Calibrate() => {
            if mstate == MState::Uncalibrated() {
                (
                    ROT_CALIBRATION_PERIOD,
                    length * 10,
                    0,
                    MState::Calibrating(true),
                )
            } else {
                (period, target, position, mstate)
            }
        }
        Msg::MoveAbs(x) => match mstate {
            MState::Stopped() => (period, x, position, MState::Starting(0)),
            MState::Starting(step) => {
                if (x - position) * (target - position) >= 0 {
                    (period, x, position, MState::Starting(step))
                } else {
                    (period, target, position, MState::Stopping(step))
                }
            }
            MState::Moving() => {
                if (x - position).abs() > (asteps as i64) {
                    (period, x, position, mstate)
                } else {
                    let p = position + dirof(target - position) * (asteps as i64);
                    let m = MState::Stopping(asteps);
                    (period, p, position, m)
                }
            }
            _ => nop,
        },
        Msg::MoveDir(Some(fwd)) => {
            if mstate == MState::Stopped() {
                let t = if fwd { length * 2 } else { 0 - length };
                let m = MState::Starting(0);
                (period, t, position, m)
            } else {
                nop
            }
        }
        Msg::MoveDir(None) => match mstate {
            MState::Starting(step) => {
                let m = MState::Stopping(step);
                (period, target, position, m)
            }
            MState::Moving() => {
                let t = position + (asteps as i64);
                let m = MState::Stopping(asteps as u64);
                (period, t, position, m)
            }
            MState::Calibrating(_) => {
                let m = MState::Uncalibrated();
                (period, position, position, m)
            }
            _ => nop,
        },
        Msg::Fake() => {
            info!("stepper2 is fake dummy");
            (period, 0, 0, MState::Dummy())
        }
        _ => handle_msg_def(msg, length, period, target, position, mstate),
    }
}

// update state
fn update_rot(
    length: i64,
    period: u64,
    asteps: u64,
    target: i64,
    position: i64,
    mstate: MState,
    s1: bool,
) -> (
    i64,
    i64,
    i64,
    MState,
    Option<bool>,
    Vec<u64>,
    Option<Duration>,
) {
    let mut v: Vec<u64> = Vec::new();
    let no_dir: Option<bool> = None;
    let no_dur: Option<Duration> = None;
    let set_mstate = |x: MState| (length, target, position, x, no_dir, v.to_vec(), no_dur);
    let set_periods = |ps: _| (length, target, position, mstate, no_dir, ps, no_dur);

    let nop = (length, target, position, mstate, None, v.to_vec(), None);
    match mstate {
        MState::Uncalibrated() => {
            thread::sleep(Duration::from_millis(10));
            nop
        }
        MState::Dummy() => {
            thread::sleep(Duration::from_millis(100));
            nop
        }
        MState::Calibrating(true) => {
            if position == 0 {
                for i in 0..asteps {
                    v.push(f_starting(
                        i,
                        asteps,
                        ROT_CALIBRATION_PERIOD,
                        ROT_STOP_PERIOD,
                    ));
                }
                (length, target, position, mstate, Some(true), v, None)
            } else {
                if s1 {
                    if position > 400 {
                        let m = MState::Calibrating(false);
                        (length, target, position, m, None, v, None)
                    } else {
                        let v = vec![ROT_CALIBRATION_PERIOD];
                        (length, target, position, mstate, None, v, None)
                    }
                } else {
                    let v = vec![ROT_CALIBRATION_PERIOD];
                    (length, target, position, mstate, None, v, None)
                }
            }
        }
        MState::Calibrating(false) => {
            if s1 {
                let v = vec![ROT_CALIBRATION_PERIOD];
                (length, target, position, mstate, None, v, None)
            } else {
                for i in (0..asteps).rev() {
                    v.push(f_stopping(
                        i,
                        asteps,
                        ROT_CALIBRATION_PERIOD,
                        ROT_STOP_PERIOD,
                    ));
                }
                (
                    length,
                    asteps as i64,
                    0,
                    MState::Stopped(),
                    None,
                    v,
                    BIG_PAUSE,
                )
            }
        }
        MState::Stopped() => (
            length,
            target,
            position,
            mstate,
            None,
            v,
            Some(Duration::from_millis(10)),
        ),
        MState::Starting(step) => update_starting(
            step,
            ROT_STOP_PERIOD,
            ROT_ACCEL,
            length,
            period,
            asteps,
            target,
            position,
            mstate,
        ),
        MState::Moving() => {
            let d = target - position;
            if d.abs() > ((asteps + SMALL_STEPS) as i64) {
                set_periods(vec![period; SMALL_STEPS as usize])
            } else if d.abs() > (asteps as i64) {
                set_periods(vec![period; (d.abs() - (asteps as i64)) as usize])
            } else {
                set_mstate(MState::Stopping(asteps))
            }
        }
        MState::Stopping(step) => {
            if step <= 0 {
                (length, position, position, MState::Stopped(), None, v, None)
            } else {
                let d = std::cmp::min(step, SMALL_STEPS);

                let v = mkv(step, step - d, ROT_ACCEL);
                let m = MState::Stopping(step - d);
                (length, target, position, m, None, v, None)
            }
        }
    }
}

impl Stepper {
    pub fn new_circular(mut dir_sw: Switch, mut pul_sw: Switch, sensor: Sensor) -> Stepper {
        let (tx_msg, rx_msg) = mpsc::channel();
        let (tx_model, rx_model) = mpsc::channel();

        thread::spawn(move || {
            core_affinity::set_for_current(core_affinity::CoreId { id: 3 });

            let mut position: i64 = 0;
            let mut target: i64 = 0;
            let mut period: u64 = 1000;
            let mut asteps: u64 = accel_steps(period, ROT_ACCEL);
            let length: i64 = 35714;
            let mut dir = false;
            let mut s1 = false;
            let mut bip_start: Option<i64> = None;

            let mut mstate = MState::Uncalibrated();
            // let mut mstate = MState::Stopped();

            loop {
                //handle messages
                if let Ok(msg) = rx_msg.try_recv() {
                    if mstate != MState::Dummy() {
                        info!("stepper2 thread got msg: {:?}", msg);
                    };
                    #[allow(irrefutable_let_patterns)]
                    if let (period_, target_, position_, mstate_) =
                        handle_msg_rot(msg, length, period, asteps, target, position, mstate)
                    {
                        if mstate != mstate_ {
                            info!("stepper2 msg new mstate: {:?}", mstate_);
                        }
                        period = period_;
                        asteps = accel_steps(period, ROT_ACCEL);
                        target = target_;
                        position = position_;
                        mstate = mstate_;
                    }
                }

                // get sensor
                if let Ok(msg) = sensor.rx.try_recv() {
                    s1 = msg;
                };

                // recalibrate
                if let MState::Moving() = mstate {
                    if s1 && target > position && bip_start == None {
                        bip_start = Some(position);
                    }
                    if !s1 && bip_start != None {
                        let s = bip_start.unwrap();
                        let bip = (length + position - s) % length;
                        let d = (length + position) % length;
                        if bip > 400 {
                            if d < 500 || length - d < 500 {
                                info!("stepper2 recalibrating at {:?}", position);
                                position = 0;
                                bip_start = None
                            } else {
                                info!("stepper2 recalibration out of range at {:?}", d);
                                bip_start = None;
                            }
                        } else {
                            info!(
                                "stepper2 recalibration too short: bip: {:?}  d: {:?}",
                                bip, d
                            );
                        }
                    }
                }

                // update model
                let (mut setdir, mut wait) = (None, None);
                let mut periods: Vec<u64> = Vec::new();
                #[allow(irrefutable_let_patterns)]
                if let (_, target_, position_, mstate_, setdir_, periods_, wait_) =
                    update_rot(length, period, asteps, target, position, mstate, s1)
                {
                    if mstate != mstate_ || (mstate == MState::Calibrating(true) && position == 0) {
                        let mut p = periods_.to_vec();
                        p.truncate(3);
                        info!(
                            "stepper2 new mstate: {:?}",
                            (target_, position_, mstate_, setdir_, p, wait_)
                        );
                    }
                    // length = length_; // no
                    target = target_;
                    position = position_;
                    mstate = mstate_;
                    setdir = setdir_;
                    periods = periods_;
                    wait = wait_;
                }

                // output model
                let model = serialize_state(length, period, target, position, mstate, s1, s1);
                tx_model.send(model).unwrap();

                // set direction
                if let Some(fwd) = setdir {
                    bip_start = None;
                    dir = fwd;
                    dir_sw.update(fwd);
                    spin_sleep::sleep(Duration::from_micros(10));
                }

                // ouput pulses
                if periods.len() > 0 {
                    for i in 0..periods.len() {
                        let p = if periods[i] < MIN_PERIOD {
                            warn!("period too small: {:?}", periods[i]);
                            MIN_PERIOD
                        } else {
                            periods[i]
                        };

                        let dp = if dir { 1 } else { -1 };

                        // pul_sw.update(false);
                        pul_sw.update(true);
                        spin_sleep::sleep(Duration::from_micros(5));
                        pul_sw.update(false);
                        spin_sleep::sleep(Duration::from_micros(p - 5 - DELAY_ADJUSTMENT));

                        position += dp;

                        if position == length && dp == 1 {
                            position = 0;
                        }
                        if position == -1 && dp == -1 {
                            position = length - 1;
                        }
                    }
                }

                // wait
                if let Some(dur) = wait {
                    thread::sleep(dur);
                }
            } // loop
        });

        return Stepper {
            tx: tx_msg,
            rx: rx_model,
        };
    }
}
