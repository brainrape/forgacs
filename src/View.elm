module View exposing (..)

import Data exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Events.Extra.Pointer exposing (..)
import Page exposing (..)
import Round


fi : Bool -> a -> a -> a
fi if_ then_ else_ =
    if if_ then
        then_

    else
        else_


pointer : Html.Attribute msg
pointer =
    style "cursor" "pointer"


arrow : Html.Attribute msg
arrow =
    style "cursor" "default"


active : Bool -> Html.Attribute msg
active bool =
    if bool then
        style "color" "darkgreen"

    else
        style "color" "grey"


large : Html.Attribute msg
large =
    style "font-size" "1.5em"


viewInt : Int -> Html Msg
viewInt x =
    span [ style "width" "80px", style "display" "inline-block" ] [ text (String.fromInt x) ]


viewFloat : Float -> Html Msg
viewFloat x =
    span [ style "width" "80px", style "display" "inline-block" ] [ text (Round.round 2 x) ]


viewSlider : Int -> Html Msg
viewSlider x =
    div [] [ text (Debug.toString x) ]


viewBool : Bool -> Html Msg
viewBool bool =
    span [ large, arrow, active bool ] [ text (fi bool "⬤" "◯") ]


viewBoolBtn : Bool -> ControlMsg -> ControlMsg -> Html Msg
viewBoolBtn bool msgOn msgOff =
    span [ large, pointer, active bool, onClick (Control (fi bool msgOff msgOn)) ] [ text (fi bool "⬤" "◯") ]


viewDir : Id -> Maybe Bool -> (Id -> Maybe Bool -> ControlMsg) -> Html Msg
viewDir id dir msg =
    let
        btn_ bool controlMsg on off =
            span [ large, pointer, active bool, onClick (Control controlMsg) ] [ text (fi bool on off) ]
    in
    span []
        [ btn_ (dir == Just False) (msg id (Just False)) "◀" "◁"
        , btn_ (dir == Nothing) (msg id Nothing) "■" "□"
        , btn_ (dir == Just True) (msg id (Just True)) "▶" "▷"
        ]


viewStepDir : Id -> Maybe Bool -> (Id -> Maybe Bool -> ControlMsg) -> Html Msg
viewStepDir id dir msg =
    let
        btn_ bool controlMsg on off =
            span
                [ large
                , pointer
                , active bool
                , onDown (\_ -> Control controlMsg)
                , onUp (\_ -> Control (msg id Nothing))
                ]
                [ text (fi bool on off) ]
    in
    span []
        [ btn_ (dir == Just False) (msg id (Just False)) "◀" "◁"
        , text " "
        , btn_ (dir == Just True) (msg id (Just True)) "▶" "▷"
        ]


viewRow : String -> String -> List Int -> List (Html Msg) -> Html Msg
viewRow stage title gpios ops =
    tr []
        [ --td [] [text stage],
          td [] [ text title ]
        , td [] [ text (String.join ", " (List.map String.fromInt gpios)) ]

        --, td [ style "text-align" "center" ] [ content ]
        , td [] ops
        ]



-- viewStepper : Id -> Html Msg
-- viewStepper id =
--     let
--         btn_ msg text_ =
--             span [ large, pointer, onClick (Control msg) ] [ text text_ ]
--     in
--     span []
--         [ span []
--             [ btn_ (Step id -1000) "↞"
--             , btn_ (Step id -100) "⯬"
--             , btn_ (Step id -1) "🢐"
--             , btn_ (Step id 1) "🢒"
--             , btn_ (Step id 100) "⯮"
--             , btn_ (Step id 1000) "↠"
--             ]
--         ]


viewStepper : Id -> Int -> MotorState -> List (Html Msg)
viewStepper id period dir =
    [ viewStepDir id dir StepperMoveDir
    , input
        [ onInput (Page.StepPeriod id)
        , type_ "range"
        , Html.Attributes.min "2.3"
        , Html.Attributes.max "4.0"
        , Html.Attributes.step "0.01"
        , value (String.fromFloat (logBase 10 (toFloat period)))
        ]
        []
    , text " | period:"
    , text (String.fromInt period)

    --, button [] [ text "reset", onClick (Control (StepperCalibrate id)) ]
    ]


viewServo : Id -> Float -> List (Html Msg)
viewServo id val =
    [ input
        [ onInput (Page.Servo id)
        , type_ "range"
        , Html.Attributes.min "0"
        , Html.Attributes.max "1"
        , Html.Attributes.step "0.01"
        , value (String.fromFloat val)
        ]
        []
    , viewFloat val
    ]


btn : ControlMsg -> String -> Html Msg
btn msg txt =
    button [ onClick (Control msg) ] [ text txt ]


linearStepper : State -> List (Html Msg)
linearStepper state =
    [ tr [] [ th [ style "text-align" "left" ] [ text "Linear Stepper" ] ]
    , viewRow "Steppers"
        "DIR, PUL"
        [ 21, 20 ]
        (viewStepper 1 state.stepper1.period state.stepper1.dir)
    , viewRow ""
        "BWD, FWD Sensors"
        [ 2, 3 ]
        [ viewBool state.stepper1.sensor1, viewBool state.stepper1.sensor2 ]

    --, viewRow "" "" [] [ btn (StepperCalibrate 1) "calibrate", btn (StepperPlay 1) "play" ]
    , tr []
        [ td [ colspan 5 ]
            [ text "State"
            , text " { calibrated: "
            , viewBool state.stepper1.calibrated
            , text " | calibrating: "
            , viewBool state.stepper1.calibrating

            -- , br [] []
            , text " | length: "
            , text (Debug.toString state.stepper1.length)
            , text " | target: "
            , text (Debug.toString state.stepper1.target)
            , text " | position: "
            , text (Debug.toString state.stepper1.position)
            , text " }"
            ]
        ]
    ]


circularStepper : State -> List (Html Msg)
circularStepper state =
    [ tr [] [ th [ style "text-align" "left" ] [ text "Circular Stepper" ] ]
    , viewRow ""
        ": DIR, PUL. "
        [ 10, 11 ]
        (viewStepper 2 state.stepper2.period state.stepper2.dir)
    , viewRow ""
        ": Sensor"
        [ 4 ]
        [ viewBool state.stepper2.sensor1 ]
    , tr []
        [ td [ colspan 5 ]
            [ text " | calibrated: "
            , viewBool state.stepper2.calibrated
            , text " | calibrating: "
            , viewBool state.stepper2.calibrating
            , br [] []
            , text " | length: "
            , text (Debug.toString state.stepper2.length)
            , text " | position: "
            , text (Debug.toString state.stepper2.position)
            , text " | target: "
            , text (Debug.toString state.stepper2.target)
            ]
        ]

    --, viewRow "" "" [] [ btn (StepperCalibrate 2) "calibrate", btn (StepperPlay 2) "play" ]
    ]


viewSystem : State -> Html Msg
viewSystem state =
    table []
        ([ tr []
            [ th [] []
            , th [ style "text-align" "left" ] [ text "GPIOs" ]
            , th [] []

            --, th [] []
            , th [] []
            ]
         , tr [] [ th [ style "text-align" "left" ] [ text "Inputs" ] ]
         , fi (state.hostname == "forgacs2") (text "") (viewRow "Sensors" "Sensor 1 (Button)" [ 7, 6 ] [ viewBool state.sensor1.state ])
         , fi (state.hostname /= "forgacs2") (text "") (viewRow "" "Sensor 2 (Video Sync)" [ 8 ] [ viewBool state.sensor2.state ])
         , fi (state.hostname /= "forgacs3") (text "") (tr [] [ th [ style "text-align" "left" ] [ text "Outputs" ] ])
         , fi (state.hostname /= "forgacs2") (text "") (viewRow "Servos" "Servo1" [ 1 ] (viewServo 1 state.servo1.position))
         , fi (state.hostname /= "forgacs3")
            (text "")
            (viewRow ""
                "Switch 1 (Video Sync)"
                [ 15 ]
                [ viewBoolBtn state.switch1.state (Switch 1 True) (Switch 1 False) ]
            )

         -- , viewRow "" "Switch 2" [ 16 ] (viewBoolBtn state.switch2 (Switch 1 True) (Switch 1 False)) []
         , tr [] [ th [ style "text-align" "left" ] [ text "Videos" ] ]
         , viewRow "" "video.mp4" [] [ viewBool state.video1.playing, text " ", btn (VideoPlay 1) "Play", text " ", btn (VideoStop 1) "Stop", text " lentgh: ", text <| String.fromInt state.video1.duration ]
         ]
            ++ fi state.stepper1.exists (linearStepper state) []
            ++ fi state.stepper2.exists (circularStepper state) []
        )


viewUrl : String -> Html msg
viewUrl ip =
    let
        url =
            "https://" ++ ip ++ ":2220/x5q6rzvd/"
    in
    a [ href url ] [ text url ]


view : Model -> Html Msg
view model =
    div [ style "user-select" "none" ]
        [ node "style" [] [ text "* { font-family: sans; }" ]
        , node "meta" [ attribute "name" "viewport", attribute "content" "width=device-width, initial-scale=1" ] []
        , case model.state of
            Ok state ->
                div []
                    [ div []
                        [ text "hostname: "
                        , b [] [ text state.hostname ]
                        , text " ip: "
                        , b [] [ text state.ip ]
                        , text " url: "
                        , viewUrl state.ip
                        ]
                    , div []
                        [ --, btn GoCalibrate "Calibrate"
                          text "Performance: "
                        , btn GoPlay "Start"
                        , btn GoReset "Reset"
                        , btn GoInit "Emergency Stop"
                        , text " Stage: "
                        , text (Debug.toString state.stage)
                        ]
                    , br [] []
                    , viewSystem state
                    , br [] []

                    -- , br [] []
                    -- , img [ style "max-width" "800px", src "https://www.raspberrypi-spy.co.uk/wp-content/uploads/2012/06/Raspberry-Pi-GPIO-Header-with-Photo.png" ] []
                    ]

            Err err ->
                if model == initModel then
                    text "Loading ..."

                else
                    div []
                        [ text ("Error: " ++ err)
                        , br [] []
                        , button [ onClick Reload ] [ text "Reload" ]
                        ]
        , div [ style "display" "none" ]
            [ text "Debug:"
            , br [] []
            , text (Debug.toString model)
            ]
        ]
