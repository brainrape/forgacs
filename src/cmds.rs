use std::process::Command;
use std::time::Duration;

pub fn set_pulldown(pin: u8) {
    Command::new("raspi-gpio")
        .args(&["set", pin.to_string().as_str(), "pd"])
        .output()
        .expect("failed to set pullup");
}

pub fn set_pullup(pin: u8) {
    Command::new("raspi-gpio")
        .args(&["set", pin.to_string().as_str(), "pu"])
        .output()
        .expect("failed to set pullup");
}

// pub fn play(filename: &str) {
//     info!("cmds::play {:?}", Command::new("omxplayer")
//         .args(&[filename, "--loop"])
//         .output()
//         .expect("failed to play video"));
// }

pub fn stop() {
    Command::new("killall")
        .args(&["-9", "omxplayer.bin"])
        .output()
        .expect("failed to stop video");
}

pub fn get_video_duration(filename: &str) -> Option<Duration> {
    let result = Command::new("ffprobe")
        .args(&[
            "-v",
            "error",
            "-show_entries",
            "format=duration",
            "-of",
            "default=noprint_wrappers=1:nokey=1",
            filename,
        ])
        .output();

    if let Ok(o) = result {
        if let Ok(s) = std::str::from_utf8(&o.stdout) {
            let r: Result<f64, _> = s.trim().parse();
            if let Ok(x) = r {
                return Some(Duration::from_millis(1000 * (x - 1.0) as u64));
            }
        }
    }
    return None;
}

pub fn get_ip() -> String {
    let result = Command::new("hostname").args(&["-I"]).output();

    if let Ok(o) = result {
        if let Ok(s) = std::str::from_utf8(&o.stdout) {
            return String::from(s.trim());
        }
    }
    return String::from("");
}
